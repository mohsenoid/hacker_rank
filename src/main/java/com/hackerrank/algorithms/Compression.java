package com.hackerrank.algorithms;

import java.io.IOException;

/**
 * Created by Mohsen on 4/29/16.
 */
public class Compression {

    public static void main(String[] args) throws IOException {
//        Scanner in = new Scanner(System.in);
//        final String fileName = System.getenv("OUTPUT_PATH");
//        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
//        String res;
//        String _str;
//        try {
//            _str = in.nextLine();
//        } catch (Exception e) {
//            _str = null;
//        }
//
//        res = compress(_str);
//        bw.write(res);
//        bw.newLine();
//
//        bw.close();

        String res1 = compress("aaaaaabbbbbbbbbbcccccccccdeeeeeee");
        String res2 = compress("aaaaabbbbbbbbbccccpqrstuv");
    }


    static String compress(String str) {
        String result = "";

        while (str.length() > 0) {
            char c = str.charAt(0);
            int i = 1;
            while (i < str.length() && str.charAt(i) == c)
                i++;

            if (i == 1)
                result += c;
            else
                result += c + "" + i;

            str = str.substring(i);
        }

        return result;
    }
}
