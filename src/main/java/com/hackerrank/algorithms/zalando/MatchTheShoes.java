package com.hackerrank.algorithms.zalando;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Mohsen on 05/06/16.
 */
public class MatchTheShoes {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int k = in.nextInt();
        int m = in.nextInt();
        int n = in.nextInt();

        ArrayList<Integer> array = new ArrayList<Integer>();

        for (int i = 0; i < n; i++) {
            array.add(in.nextInt());
        }

        Map<Integer, Long> counts = array.stream()
                .sorted((o1, o2) -> Integer.compare(o1, o2))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        counts.entrySet().stream()
                .sorted((o1, o2) -> Long.compare(o2.getValue(), o1.getValue()))
                .limit(k)
                .forEach(integerLongEntry -> System.out.println(integerLongEntry.getKey()));

        in.close();
    }
}
