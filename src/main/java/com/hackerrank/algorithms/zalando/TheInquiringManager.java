package com.hackerrank.algorithms.zalando;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Mohsen on 05/06/16.
 */
public class TheInquiringManager {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int[][] rows = new int[n][2];

        for (int i = 0; i < n; i++) {
            int type = in.nextInt();
            switch (type) {
                case 1:
                    rows[i][1] = in.nextInt();
                    rows[i][0] = in.nextInt();
                    break;
                case 2:
                    inquire(rows, in.nextInt(), i);
                    break;
            }
        }
        in.close();
    }

    private static void inquire(int[][] rows, int inquireTime, int time) {
        int max = -1;
        for (int i = time; i >= 0; i--) {
            if (rows[i][1] != 0) {
                if (inquireTime - rows[i][0] >= 60) break;
                if (rows[i][1] > max) max = rows[i][1];
            }
        }
        System.out.println(max);
    }
}
