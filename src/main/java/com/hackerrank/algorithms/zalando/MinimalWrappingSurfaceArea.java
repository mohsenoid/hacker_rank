package com.hackerrank.algorithms.zalando;

import java.util.Scanner;

/**
 * Created by Mohsen on 05/06/16.
 */
public class MinimalWrappingSurfaceArea {

    static class Box {
        public int w, l, h;

        public Box(int w, int l, int h) {
            this.w = w;
            this.l = l;
            this.h = h;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();

        Box box = new Box(in.nextInt(), in.nextInt(), in.nextInt());

        long result = Long.MAX_VALUE;

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                for (int k = 1; k <= n; k++) {
                    if (i * j * k < n) continue;

                    long nw = box.w * i;
                    long nl = box.l * j;
                    long nh = box.h * k;

                    long area = 2 * (nw * nl + nl * nh + nh * nw);

                    if (area < result)
                        result = area;
                }
            }
        }

        System.out.println(result);
    }
}
